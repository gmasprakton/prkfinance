<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\IndexController;
use SugarAPI\SDK\SugarAPI;

/**
 * This is the factory for IndexController. Its purpose is to instantiate the
 * controller and inject dependencies into it.
 */
class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
    	$authenticationService = $container->get(\Zend\Authentication\AuthenticationService::class);
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $instance = 'https://prk-finance.prakton.es/crm/rest/v10/';
    	$auth = array(
    		'username' => 'api_portal',
    		'password' => 'Defuser5$',
    		'platform' => 'api',
    	);
    	$sugar = new SugarAPI($instance, $auth);	
    	try{
    		$sugar->login();
    	}catch(SugarAPI\SDK\Exception\Authentication\AuthenticationException $ex){
			echo "Failed to authenticate to server. Message ".$ex->getMessage();
			die();
    	}
        // Instantiate the controller and inject dependencies
        return new IndexController($entityManager, $sugar, $authenticationService);
    }
}