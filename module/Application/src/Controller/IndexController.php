<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Entity\User;

/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class IndexController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    private $sugar;
    private $authService;
    private $_isProvider = false;
    private $_isClient = false;

    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $sugar, $authService) 
    {
       $this->authService = $authService;
       $this->entityManager = $entityManager;
       $this->sugar = $sugar;

       //Fetch User Type 
        $this->_isProvider = false;
        $identity = $this->authService->getIdentity();
        if(!empty($identity)){
            $client = $this->getClientbyEmail($identity);
            if(in_array('2', $client['account_type'])){
                $this->_isProvider = true;
            }

            if(in_array('1', $client['account_type'])){
                $this->_isClient = true;
            }
        }
    }
    
    /**
     * This is the default "index" action of the controller. It displays the 
     * Home page.
     */
    public function indexAction() 
    {
        //return new ViewModel();
        return $this->redirect()->toRoute('login');
    }

    /**
     * This is the "about" action. It is used to display the "About" page.
     */
    public function aboutAction() 
    {              
        $appName = 'PRK Finance';
        $appDescription = 'This demo shows how to implement user management with Zend Framework 3';
        
        // Return variables to view script with the help of
        // ViewObject variable container
        return new ViewModel([
            'appName' => $appName,
            'appDescription' => $appDescription,
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ]);
    }  
    
    /**
     * The "settings" action displays the info about currently logged in user.
     */
    public function settingsAction()
    {
        // Use the CurrentUser controller plugin to get the current user.
        $user = $this->currentUser();
        $emailUser = $this->authService->getIdentity();
        //Si no tiene Email en sessión es que no se ha logueado
        if ($user==null) {
            throw new \Exception('Not logged in');
        }
        if(!empty($emailUser))
        {
            $idClient = $this->getClientbyEmail($emailUser);
        }
        
        return new ViewModel([
            'user' => $user,
            'client' => $idClient,
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ]);
    }

    private function getClientbyEmail($email = '')
    {
        if(!empty($email))
        {   
            $FilterEndpoint = $this->sugar->filterRecords('Accounts');
            $data = array(
                'max_num' => 1,
                'filter' => array(
                    array(
                        '$or' => array(
                            array(
                                //name starts with 'a'
                                "email1" => array(
                                    '$starts' => $email,
                                )
                            )
                        ),
                    ),
                ),
                'fields' => array("name", "cif_c", "account_type")
            );
            $response = $FilterEndpoint->execute($data)->getResponse();

            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    echo $response->getBody();
                }else{
                   echo $response->getError();
                }
            }
            return (!empty($datos['records'][0])) ? $datos['records'][0] : '';
        }else{
            return null;
        }
    }
}

