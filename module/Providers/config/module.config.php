<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Providers;

//use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router' => [
        'routes' => [
            'solicitar' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/finance/solicitar[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'aplazafactura' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/finance/aplazafactura[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'addInvoice' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/finance/addfactura[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'finance' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/finance[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'no_access' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/no_access',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'noaccess',
                    ],
                ],
            ], 
            'error' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/error',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'error',
                    ],
                ],
            ],         
        ],
    ],
    'controllers' => [
        'factories' => [
            //Controller\IndexController::class => InvokableFactory::class,
            Controller\IndexController::class => Controller\Factory\ProviderControllerFactory::class,            
        ],
    ],
    'service_manager' => [
        'factories' => [
            \Zend\Authentication\AuthenticationService::class => Service\Factory\AuthenticationServiceFactory::class,
            \User\Service\AuthAdapter::class => \User\Service\Factory\AuthAdapterFactory::class,
            \User\Service\AuthManager::class => \User\Service\Factory\AuthManagerFactory::class,
            \User\Service\UserManager::class => \User\Service\Factory\UserManagerFactory::class,
            Service\ProviderManager::class => Service\Factory\ProviderManagerFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            \User\View\Helper\CurrentUser::class => \User\View\Helper\Factory\CurrentUserFactory::class,
            \Zend\View\Helper\Service\FlashMessenger::class => \Zend\View\Helper\Service\FlashMessengerFactory::class,
        ],
        'aliases' => [
            'currentUser' => \User\View\Helper\CurrentUser::class,
        ],
    ],
    'view_helpers_config' => [
        'flashmessenger' => [
            'message_open_format'      => '<div%s><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><ul><li>',
            'message_close_string'     => '</li></ul></div>',
            'message_separator_string' => '</li><li>',
        ],
    ],
    'access_filter' => [
        'controllers' => [
            Controller\IndexController::class => [
                // Give access to "index", "add", "edit", "view", "changePassword" actions to authorized users only.
                ['actions' => ['index'], 'allow' => '@']
            ],
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'providers/index/index' => __DIR__ . '/../view/providers/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],  
];
