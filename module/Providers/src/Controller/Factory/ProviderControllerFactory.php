<?php
namespace Providers\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use User\Controller\UserController;
use User\Service\UserManager;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;
use Providers\Controller\IndexController;
use SugarAPI\SDK\SugarAPI;
/**
 * This is the factory for UserController. Its purpose is to instantiate the
 * controller and inject dependencies into it.
 */
class ProviderControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
    	$instance = 'https://prk-finance.prakton.es/crm/rest/v10/';
    	$auth = array(
    		'username' => 'api_portal',
    		'password' => 'Defuser5$',
    		'platform' => 'api',
    	);
    	$sugar = new SugarAPI($instance, $auth);	
    	try{
    		$sugar->login();
    	}catch(SugarAPI\SDK\Exception\Authentication\AuthenticationException $ex){
			echo "Failed to authenticate to server. Message ".$ex->getMessage();
			die();
    	}
		
		$authenticationService = $container->get(\Zend\Authentication\AuthenticationService::class);
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        
        $sessionManager = $container->get(SessionManager::class);

        // Instantiate the controller and inject dependencies
        return new IndexController($entityManager, $userManager, $sugar, $authenticationService, $sessionManager);
    }
}