<?php
namespace Providers\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Entity\User;
use Providers\Form\ProviderForm;
use Providers\Form\LiquidationForm;
use Providers\Form\InvoiceForm;


/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class IndexController extends AbstractActionController 
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;
    private $userManager;
    private $sugar;
    private $authService;
    private $session;

    private $_isProvider = false;
    private $_isClient = false;

    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager, $userManager, $sugar, $authService, $sessionManager) 
    {
        $this->authService = $authService;
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->sugar = $sugar;
        $this->session = $sessionManager;

        //Fetch User Type 
        $this->_isProvider = false;
        $identity = $this->authService->getIdentity();
        if(!empty($identity)){
            $client = $this->getClientbyEmail($identity);
            if(in_array('2', $client['account_type'])){
                $this->_isProvider = true;
            }

            if(in_array('1', $client['account_type'])){
                $this->_isClient = true;
            }
        }
    }
    
    /**
     * This is the default "index" action of the controller. It displays the 
     * Home page.
     */
    public function indexAction() 
    {
        $emailUser = $this->authService->getIdentity();

        //Si no tiene Email en sessión es que no se ha logueado
        if(!empty($emailUser))
        {
            $idClient = $this->getClientbyEmail($emailUser);
        }else{
            return $this->redirect()->toRoute('login');    
        }

        if(!empty($idClient['id']))
        {
            $facturas = $this->getInvoices($idClient['id']);
            $numFacturas = count($facturas['records']);

            $facturasClient = $this->getInvoicesClient($idClient['id']);
            $numFacturasClient = count($facturas['records']);

            $facturasHistoric = $this->getInvoicesHistoric($idClient['id']);
            $numFacturasHistoric = count($facturasHistoric['records']);
        }else{
            return new ViewModel(array(
                'facturas' => '',
                'client' => '',
                'historicFacturas' => '',
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
        }

        /*
        Client:
            1- Client
            2- Proveedor
        */
        if(in_array("2", $idClient['account_type'])){
            return new ViewModel(array(
                'facturas' => $facturas['records'],
                'historicFacturas' => $facturasHistoric['records'],
                'client' => $idClient,
                'numFacturas' => $numFacturas,
                'numFacturasClient' => $numFacturasClient,
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
        }else{
            return $this->redirect()->toRoute('finance', 
                        ['action'=>'client']);
        }
    }

    public function noaccessAction()
    {
        $emailUser = $this->authService->getIdentity();
        if(!empty($emailUser)){
            $idClient = $this->getClientbyEmail($emailUser);
            return new ViewModel(array(
                'client' => $idClient,
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
        }else{
            return $this->redirect()->toRoute('login');
        }
    }

    public function errorAction()
    {
        $emailUser = $this->authService->getIdentity();
        if(!empty($emailUser)){
            $idClient = $this->getClientbyEmail($emailUser);
            return new ViewModel(array(
                'client' => $idClient,
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
        }else{
            return $this->redirect()->toRoute('login');
        }
    }

    public function legalAction()
    {
        $emailUser = $this->authService->getIdentity();
        if(!empty($emailUser)){
            $idClient = $this->getClientbyEmail($emailUser);
            return new ViewModel(array(
                'client' => $idClient,
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
        }else{
            return $this->redirect()->toRoute('login');
        }
    }

    public function cookiesAction()
    {
        $emailUser = $this->authService->getIdentity();
        if(!empty($emailUser)){
            $idClient = $this->getClientbyEmail($emailUser);
            return new ViewModel(array(
                'client' => $idClient,
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
        }else{
            return $this->redirect()->toRoute('login');
        }
    }

    public function datosAction()
    {
        $emailUser = $this->authService->getIdentity();
        if(!empty($emailUser)){
            $idClient = $this->getClientbyEmail($emailUser);
            return new ViewModel(array(
                'client' => $idClient,
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
        }else{
            return $this->redirect()->toRoute('login');
        }
    }

    public function clientAction() 
    {
        $emailUser = $this->authService->getIdentity();
        //Si no tiene Email en sessión es que no se ha logueado
        if(!empty($emailUser))
        {
            $idClient = $this->getClientbyEmail($emailUser);
        }else{
            return $this->redirect()->toRoute('login');    
        }
        if(!empty($idClient['id']))
        {
            $facturas = $this->getInvoices($idClient['id']);
            $numFacturas = count($facturas['records']);
            $totalAmountFacturas = $this->_totalImportInvoices($facturas);

            $facturasClient = $this->getInvoicesClient($idClient['id']);
            $numFacturasClient = count($facturasClient['records']);

            $facturasClientCredito = $this->getInvoicesClientCredito($idClient['id']);
            $totalAmountFacturasClient = $this->_totalImportInvoices($facturasClientCredito);
            $totalCreditAvaible = $idClient['credito_c'] - $totalAmountFacturasClient;

            $facturasHistoric = $this->getInvoicesHistoricCliente($idClient['id']);
            $numFacturasHistoric = count($facturasHistoric['records']);

        }else{
            return new ViewModel(array(
                'facturas' => '',
                'historicFacturas' => '',
                'client' => '',
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
        }

        /*
        Client:
            1- Client
            2- Proveedor
        */
        return new ViewModel(array(
            'facturas' => $facturasClient['records'],
            'historicFacturas' => $facturasHistoric['records'],
            'client' => $idClient,
            'totalAmountFacturasClient' => $totalAmountFacturasClient,
            'totalCreditAvaible' => $totalCreditAvaible,
            'numFacturas' => $numFacturas,
            'numFacturasClient' => $numFacturasClient,
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ));
    }

    public function solicitarAction()
    {
        $idInvoice = $this->params()->fromRoute('id', -1);
        $emailUser = $this->authService->getIdentity();
        
        //Check ownership
        $hasPermissions = $this->_hasPermissionsForRecord($idInvoice);
        if(!$hasPermissions){
            return $this->redirect()->toRoute('no_access');
            exit;
        }

        $infoInvoice = $this->getInvoice($idInvoice);
        $client = $this->getClientbyEmail($emailUser);
        $clientClient = $this->getClientbyId($infoInvoice['accounts_fac_facturas_1accounts_ida']);
        $clientProvider = $this->getClientbyId($infoInvoice['accounts_fac_facturas_2accounts_ida']);
        $matrizPor = $this->conditions($infoInvoice['importe_c'], '', $infoInvoice['fecha_vencimiento_c'], $clientClient);
        //calculo
        $TIN = '4.95';
        $dateNow = new \DateTime();
        $dateVencimiento = new \DateTime($infoInvoice['fecha_vencimiento_c']);
        $interval = $dateNow->diff($dateVencimiento);
        $diasInterval = $interval->format('%a');

        $importeTIN = round((($infoInvoice['importe_c'] * ($TIN/100))/365),2,PHP_ROUND_HALF_UP)*$diasInterval;
        $numComision = $matrizPor - $TIN;
        $comAbertura = round((($infoInvoice['importe_c'] * ($numComision/100))/365),2,PHP_ROUND_HALF_UP)*$diasInterval;

        $totalComision = $importeTIN + $comAbertura;
        $retencion = ($totalComision*19)/100; //19% Retención
        $impTotalLiquidar = $infoInvoice['importe_c'] - $totalComision;
        $impTotalLiquidar = $impTotalLiquidar + $retencion;

        $form = new LiquidationForm();
        if($this->getRequest()->isPost()){
            $dataForm = $this->params()->fromPost();
            $form->setData($dataForm);
            if(!$form->isValid()) {
                $errors = $form->getMessages();
                $this->flashMessenger()->setNamespace('error');
                foreach($errors as $field => $error){
                    $this->flashMessenger()->addMessage('El campo ' . $form->get($field)->getLabel() . ' es requerido');
                }
            } else {
                $params = array(
                    'name' => 'Operation Portal',
                    'description' => 'Desde Portal',
                    'estado_c' => 'en_proceso',
                    'fecha_liquidacion_c' => date("Y-m-d"),
                    'id_operacion_c' => 'OP_999',
                    'por_tin_c' => '4.95',
                    'total_tin_c' => $dataForm['impTIN'],
                    'por_comision_abertura_c' => $dataForm['porComision'],
                    'total_comision_abertura_c' => $dataForm['comAbertura'],
                    'importe_neto_abono_c' => $dataForm['impTotalLiquidar'],
                    'importe_administrativo_c' => $dataForm['impAdministrativo'],
                    'acepta_contrato_c' => 1,
                    'iban_c' => $dataForm['iban_c'],
                    'ip_address_c' => $_SERVER['REMOTE_ADDR'],
                    'tipo_operacion_c' => 'anticipo',
                    'retencion_19_c' => $dataForm['retencion'],
                );

                $createOperation = $this->createOperation($params);
                
                if(!empty($createOperation['id']))
                {
                    $createRelated = false;
                    $createRelated = $this->createRelatedOperation($createOperation['id'], $idInvoice, $clientProvider['id']);
                    $params = array(
                        'estado_c' => 'solicitud_anticipo',
                    );
                    $updateInvoice = $this->updateRecord($idInvoice, $params);

                    if($createRelated && $updateInvoice){
                        $view = new ViewModel(array(
                            'client' => $client,
                            'isProvider' => $this->_isProvider,
                            'isClient' => $this->_isClient,
                        ));
                        $view->setTemplate('providers/index/success-liquidation');
                        return $view;
                    }

                }
            }
        }

        return new ViewModel(array(
            'client' => $client,
            'provider' => $clientProvider,
            'idInvoice' => $idInvoice,
            'infoInvoice' => $infoInvoice,
            'TIN' => $TIN,
            'impTIN' => round($importeTIN,2,PHP_ROUND_HALF_UP),
            'porComision' => round($numComision,2,PHP_ROUND_HALF_UP),
            'comAbertura' => round($comAbertura,2,PHP_ROUND_HALF_UP),
            'retencion' => round($retencion,2,PHP_ROUND_HALF_UP),
            'totalComision' => round($totalComision,2,PHP_ROUND_HALF_UP),
            'impTotalLiquidar' => round($impTotalLiquidar,2,PHP_ROUND_HALF_UP),
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ));
    }

    public function aplazafacturaAction()
    {
        $idInvoice = $this->params()->fromRoute('id', -1);
        $emailUser = $this->authService->getIdentity();
        
        //Check ownership
        $hasPermissions = $this->_hasPermissionsForRecord($idInvoice);
        if(!$hasPermissions){
            return $this->redirect()->toRoute('no_access');
            exit;
        }

        $infoInvoice = $this->getInvoice($idInvoice);
        $client = $this->getClientbyEmail($emailUser);
        $clientClient = $this->getClientbyId($infoInvoice['accounts_fac_facturas_1accounts_ida']);
        $clientProvider = $this->getClientbyId($infoInvoice['accounts_fac_facturas_2accounts_ida']);

        $matrizPor = $this->conditions($infoInvoice['importe_c'], '', $infoInvoice['fecha_vencimiento_real_c'], $clientClient);

        //calculo
        $TIN = '4.95';
        $dateNow = new \DateTime();
        $dateVencimiento = new \DateTime($infoInvoice['fecha_vencimiento_real_c']);
        $interval = $dateNow->diff($dateVencimiento);
        $diasInterval = $interval->format('%a');

        $importeTIN = round((($infoInvoice['importe_c'] * ($TIN/100))/365),2,PHP_ROUND_HALF_UP)*$diasInterval;
        $numComision = $matrizPor - $TIN;
        $comAbertura = round((($infoInvoice['importe_c'] * ($numComision/100))/365),2,PHP_ROUND_HALF_UP)*$diasInterval;

        $totalComision = $importeTIN + $comAbertura;
        $retencion = ($totalComision*19)/100; //19% Retención
        $impTotalLiquidar = $totalComision - $retencion;

        $form = new LiquidationForm(true);
        if($this->getRequest()->isPost()){
            $dataForm = $this->params()->fromPost();
            $form->setData($dataForm);
            if(!$form->isValid()) {
                $errors = $form->getMessages();
                $this->flashMessenger()->setNamespace('error');
                foreach($errors as $field => $error){
                    $this->flashMessenger()->addMessage('El campo ' . $form->get($field)->getLabel() . ' es requerido');
                }
            } else {
                $matrizPor = $this->conditions($infoInvoice['importe_c'], $infoInvoice['fecha_vencimiento_real_c'], $dataForm['new_fecha_vencimiento_c'], $clientClient);

                //calculo
                $TIN = '4.95';
                $dateNow = new \DateTime($infoInvoice['fecha_vencimiento_real_c']);
                $dateVencimiento = new \DateTime($dataForm['new_fecha_vencimiento_c']);
                $interval = $dateNow->diff($dateVencimiento);
                $diasInterval = $interval->format('%a');

                $importeTIN = round((($infoInvoice['importe_c'] * ($TIN/100))/365),2,PHP_ROUND_HALF_UP)*$diasInterval;
                $numComision = $matrizPor - $TIN;
                $comAbertura = round((($infoInvoice['importe_c'] * ($numComision/100))/365),2,PHP_ROUND_HALF_UP)*$diasInterval;

                $totalComision = $importeTIN + $comAbertura;
                $retencion = ($totalComision*19)/100; //19% Retención
                $impTotalLiquidar = $totalComision - $retencion;

                $params = array(
                    'name' => 'Operation Portal',
                    'description' => 'Desde Portal',
                    'estado_c' => 'en_proceso',
                    'fecha_liquidacion_c' => date("Y-m-d"),
                    'id_operacion_c' => 'OP_999',
                    'por_tin_c' => '4.95',
                    'total_tin_c' => $dataForm['impTIN'],
                    'por_comision_abertura_c' => $dataForm['porComision'],
                    'total_comision_abertura_c' => $dataForm['comAbertura'],
                    'importe_neto_abono_c' => $dataForm['impTotalLiquidar'],
                    'importe_administrativo_c' => $dataForm['impAdministrativo'],
                    'acepta_contrato_c' => 1,
                    //'iban_c' => $dataForm['iban_c'],
                    'ip_address_c' => $_SERVER['REMOTE_ADDR'],
                    'tipo_operacion_c' => 'aplazamiento',
                    'retencion_19_c' => $dataForm['retencion'],
                );

                $createOperation = $this->createOperation($params);
                
                if(!empty($createOperation['id']))
                {
                    $createRelated = false;
                    $clientClient= $this->getClientbyId($infoInvoice['accounts_fac_facturas_1accounts_ida']);
                    $createRelated = $this->createRelatedOperation($createOperation['id'], $idInvoice, $clientClient['id']);
                    $new_fecha_vencimiento_c = date("Y-m-d", strtotime($dataForm['new_fecha_vencimiento_c']));
                    $params = array(
                        //'estado_c' => 'solicitud_anticipo', Comentado por Enrique, 11/01/2019
                        'fecha_vencimiento_real_c' => $new_fecha_vencimiento_c,
                        'estado_cliente_c' => 'aplazada_pendiente',
                    );
                    $updateInvoice = $this->updateRecord($idInvoice, $params);

                    if($createRelated && $updateInvoice){
                        $view = new ViewModel(array(
                            'client' => $client,
                            'isProvider' => $this->_isProvider,
                            'isClient' => $this->_isClient,
                        ));
                        $view->setTemplate('providers/index/success-liquidation');
                        return $view;
                    }

                }
            }
        }

        return new ViewModel(array(
            'client' => $client,
            'provider' => $clientProvider,
            'idInvoice' => $idInvoice,
            'infoInvoice' => $infoInvoice,
            'TIN' => $TIN,
            'impTIN' => round($importeTIN,2,PHP_ROUND_HALF_UP),
            'porComision' => round($numComision,2,PHP_ROUND_HALF_UP),
            'comAbertura' => round($comAbertura,2,PHP_ROUND_HALF_UP),
            'retencion' => round($retencion,2,PHP_ROUND_HALF_UP),
            'totalComision' => round($totalComision,2,PHP_ROUND_HALF_UP),
            'impTotalLiquidar' => round($impTotalLiquidar,2,PHP_ROUND_HALF_UP),
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ));
    }

    public function addfacturaAction()
    {
        $emailUser = $this->authService->getIdentity();
        $client = $this->getClientbyEmail($emailUser);
        $providerClient = $this->getProvidersByClientId($client['id']);

        //Procesamos los proveedores
        $listProvider = array();
        foreach ($providerClient['records'] as $record => $value) {
            array_push($listProvider, array(
                    'id' => $value['id'],
                    'name' => $value['name']
                )
            );
        }
        $newProvider = (isset($this->params()->fromPost()['idProveedor']) && $this->params()->fromPost()['idProveedor'] == 'addprovider') ? true : false;
        $form = new ProviderForm($this->getRequest(), $newProvider);
        if($this->getRequest()->isPost()){
            $dataForm = $this->params()->fromPost();
            $form->setData($dataForm);
            if(!$form->isValid()) {
                $errors = $form->getMessages();
                $this->flashMessenger()->setNamespace('error');
                foreach($errors as $field => $error){
                    if(isset($error['isLessThan'])){
                        $this->flashMessenger()->addMessage($error['isLessThan']);
                    }else{
                        $this->flashMessenger()->addMessage('El campo ' . $form->get($field)->getLabel() . ' es requerido');
                    }
                }
            }else{
                $file = $this->getRequest()->getFiles()->toArray();
                if(empty($file['file_invoice']['tmp_name'])){
                    $errors = $form->getMessages();
                    $this->flashMessenger()->setNamespace('error');
                    foreach($errors as $field => $error){
                        if(isset($error['isLessThan'])){
                            $this->flashMessenger()->addMessage($error['isLessThan']);
                        }else{
                            $this->flashMessenger()->addMessage('El campo ' . $form->get($field)->getLabel() . ' es requerido');
                        }
                    }
                    $this->flashMessenger()->addMessage('Es necesario adjuntar un fichero');
                    return new ViewModel(array(
                        'form' => $form,
                        'client' => $client,
                        'listProvider' => $listProvider,
                        'isProvider' => $this->_isProvider,
                        'isClient' => $this->_isClient,
                    ));
                    exit;
                }

                $uploadDir = ROOT_PATH . '/../uploads/';
                $result = move_uploaded_file($file['file_invoice']['tmp_name'], $uploadDir . $file["file_invoice"]["name"]);
                if($result){
                    @unlink($file['file_invoice']['tmp_name']);
                }

                //Provider
                $params = array(
                    'id' => $dataForm['idProveedor'],
                    'name' => $dataForm['name'],
                    'cif_c' => $dataForm['cif_c'],
                    'email1' => $dataForm['email'],
                    'billing_address_street' => $dataForm['billing_address_street'],   
                    'billing_address_city' => $dataForm['billing_address_city'],
                    'billing_address_state' => $dataForm['billing_address_state'],
                    'billing_address_postalcode' => $dataForm['billing_address_postalcode'],
                    'billing_address_country' => $dataForm['billing_address_country'],
                    'iban_c' => $dataForm['iban'],
                    'phone_office' => $dataForm['phone'],
                    'contact_name' => $dataForm['contact'],
                );
                $provider = $this->insertOrUpdateProvider($params);

                //Relate Provider and Client 
                if(!empty($provider['id'])){
                    $this->sugar->relateRecord('Accounts', $client['id'], 'members', $provider['id'])->execute();
                }

                //Creamos contacto cuando es nuevo Proveedor
                if(!empty($provider['id']) && $dataForm['idProveedor'] == 'addprovider'){
                    $paramsContact = array(
                        'last_name' => $dataForm['contact'],
                        'phone_mobile' => $dataForm['phone'],
                    );
                    //echo '<pre>'.print_r($dataForm['contact'], TRUE).'</pre>';
                    $contact = $this->createContact($paramsContact);
                    if(!empty($contact['id'])){
                        //echo '<pre>'.print_r($contact['id'], TRUE).'</pre>';
                        $this->sugar->relateRecord('Accounts', $provider['id'], 'contacts', $contact['id'])->execute();
                    }
                }

                $contentFile = file_get_contents($uploadDir . $file["file_invoice"]["name"]);
                $fecha_factura_c = date("Y-m-d", strtotime($dataForm['fecha_factura_c']));
                $fecha_vencimiento_c = date("Y-m-d", strtotime($dataForm['fecha_vencimiento_c']));
                $params = array(
                    'name' => 'Factura Portal',
                    'description' => $dataForm['description'],
                    'estado_c' => '',
                    'estado_cliente_c' => 'pendiente', //Patch 001_Paquete_evolutivos (18_12)
                    'fecha_factura_c' => $fecha_factura_c,
                    'fecha_vencimiento_c' => $fecha_vencimiento_c,
                    'fecha_vencimiento_real_c' => $fecha_vencimiento_c, //Patch 001_Paquete_evolutivos (18_12)
                    'id_factura' => $dataForm['id_factura'],
                    'importe_c' => $dataForm['importe_c'],
                );
                $createInvoice = $this->createInvoice($params);
                $createRelated = false;
                if(!empty($createInvoice['id']))
                {
                    $createRelated = $this->createRelatedInvoice($createInvoice['id'], $dataForm['idClient'], $provider['id']);           
                    $this->sugar->updateRecord('fac_facturas', $createInvoice['id'])->execute([
                        'estado_c' => 'disponible',
                    ]);

                    if($dataForm['idProveedor'] == 'addprovider'){
                        //Enviamos el Mail de Bienvenida
                        $this->sugar->updateRecord('Accounts', $provider['id'])->execute([
                            'mail_welcome_provider_c' => '1',
                        ]);
                    }
                }

                //Create Document
                if($createRelated)
                {
                    $params = array(
                        'document_name' => $file['file_invoice']['name'],
                        'template_type' => 'invoice',
                    );
                    $createDocument = $this->createDocument($params);
                    $createRelated = $this->createRelatedDocument($createDocument['id'], $createInvoice['id']);
                    //Añadir documento al Registro Document
                    $addDocumment = $this->addDocumment($createDocument['id'], $uploadDir . $file['file_invoice']['name']);
                }
                if($createRelated && $createDocument){
                    $view = new ViewModel(array(
                        'form' => $form,
                        'client' => $client,
                        'invoice_number' => $dataForm['id_factura'],
                        'isProvider' => $this->_isProvider,
                        'isClient' => $this->_isClient,
                    ));
                    $view->setTemplate('providers/index/success-provider');
                    return $view;
                }
            }
        }

        return new ViewModel(array(
            'form' => $form,
            'client' => $client,
            'listProvider' => $listProvider,
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ));
    }

    public function editfacturaAction()
    {
        $idInvoice = $this->params()->fromRoute('id', -1);
        $emailUser = $this->authService->getIdentity();
        
        //Check ownership
        $hasPermissions = $this->_hasPermissionsForRecord($idInvoice);
        if(!$hasPermissions){
            return $this->redirect()->toRoute('no_access');
            exit;
        }

        $infoInvoice = $this->getInvoice($idInvoice);
        $tempData = [
            'importe_c' => $infoInvoice['importe_c'],
            'id_factura' => $infoInvoice['id_factura'],
            'fecha_factura_c' => $infoInvoice['fecha_factura_c'],
            'fecha_vencimiento_c' => $infoInvoice['fecha_vencimiento_c'],
        ];
        $date = \DateTime::createFromFormat('Y-m-d', $tempData['fecha_factura_c']);
        $tempData['fecha_factura_c'] = $date->format('d-m-Y');
        $date = \DateTime::createFromFormat('Y-m-d', $tempData['fecha_vencimiento_c']);
        $tempData['fecha_vencimiento_c'] = $date->format('d-m-Y');

        $client = $this->getClientbyEmail($emailUser);

        $form = new InvoiceForm($this->getRequest());
        $form->setData($tempData);
        if($this->getRequest()->isPost()){
            $dataForm = $this->params()->fromPost();
            $form->setData($dataForm);
            if(!$form->isValid()) {
                $errors = $form->getMessages();
                $this->flashMessenger()->setNamespace('error');
                foreach($errors as $field => $error){
                    if(isset($error['isLessThan'])){
                        $this->flashMessenger()->addMessage($error['isLessThan']);
                    }else{
                        $this->flashMessenger()->addMessage('El campo ' . $form->get($field)->getLabel() . ' es requerido');
                    }
                }
            }else{
                $fecha_factura_c = date("Y-m-d", strtotime($dataForm['fecha_factura_c']));
                $fecha_vencimiento_c = date("Y-m-d", strtotime($dataForm['fecha_vencimiento_c']));
                $this->sugar->updateRecord('fac_facturas', $idInvoice)->execute([
                    'fecha_factura_c' => $fecha_factura_c,
                    'fecha_vencimiento_c' => $fecha_vencimiento_c,
                    'fecha_vencimiento_real_c' => $fecha_vencimiento_c,
                    'id_factura' => $dataForm['id_factura'],
                    'importe_c' => $dataForm['importe_c'],
                ]);

                $view = new ViewModel(array(
                    'form' => $form,
                    'client' => $client,
                    'invoice_number' => $dataForm['id_factura'],
                    'isProvider' => $this->_isProvider,
                    'isClient' => $this->_isClient,
                ));
                $view->setTemplate('providers/index/success-invoice');
                return $view;
            }
        }

        return new ViewModel([
            'form' => $form,
            'client' => $client,
            'invoice_id' => $idInvoice,
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ]);
    }

    public function deletefacturaAction()
    {
        $idInvoice = $this->params()->fromRoute('id', -1);
        $emailUser = $this->authService->getIdentity();
        
        //Check ownership
        $hasPermissions = $this->_hasPermissionsForRecord($idInvoice);
        if(!$hasPermissions){
            return $this->redirect()->toRoute('no_access');
            exit;
        }
        $infoInvoice = $this->getInvoice($idInvoice);
        if($this->getRequest()->isPost()){
            $this->sugar->updateRecord('fac_facturas', $idInvoice)->execute([
                'estado_c' => 'eliminada',
            ]);

            $view = new ViewModel(array(
                'form' => $form,
                'client' => $client,
                'invoice_number' => $infoInvoice['id_factura'],
                'isProvider' => $this->_isProvider,
                'isClient' => $this->_isClient,
            ));
            $view->setTemplate('providers/index/success-invoice-delete');
            return $view;
        }

        return new ViewModel([
            'form' => $form,
            'client' => $client,
            'invoice_id' => $idInvoice,
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ]);
    }

    public function conditionsAction()
    {
        $FilterEndpoint = $this->sugar->getAttachment('Documents', '96804928-fd26-11e8-903b-005056ab4ba0', 'filename');
        $response = $FilterEndpoint->execute()->getResponse();

        if ($response->getStatus()=='200'){
            $datos = $response->getBody();

            $response = $this->getResponse();
            $headers  = $response->getHeaders();
            $headers->addHeaderLine('Content-Type', 'application/octet-stream')
                ->addHeaderLine('Content-Disposition', "attachment; filename=Condiciones del Servicio.pdf")
                ->addHeaderLine("Pragma: no-cache")
                ->addHeaderLine("Content-Transfer-Encoding: UTF-8");

            $response->setContent($datos);
            return $response;
        }else{
            return $this->redirect()->toRoute('error');
        }

    }

    public function submitAction()
    {
        $idInvoice = $this->params()->fromRoute('id', -1);
        if ($this->getRequest()->isPost()) {           
            $dataForm = $this->params()->fromPost();            
        } 
        $infoInvoice = $this->getInvoice($idInvoice);
        $client = $this->getClientbyId($infoInvoice['accounts_fac_facturas_1accounts_ida']);
        $clientProvider = $this->getClientbyId($infoInvoice['accounts_fac_facturas_2accounts_ida']);
        $params = array(
            'name' => 'Operation Portal',
            'description' => 'Desde Portal',
            'estado_c' => 'en_proceso',
            'fecha_liquidacion_c' => date("Y-m-d"),
            'id_operacion_c' => 'OP_999',
            'por_tin_c' => '4.95',
            'total_tin_c' => $dataForm['impTIN'],
            'por_comision_abertura_c' => $dataForm['porComision'],
            'total_comision_abertura_c' => $dataForm['comAbertura'],
            'importe_neto_abono_c' => $dataForm['impTotalLiquidar'],
            'importe_administrativo_c' => $dataForm['impAdministrativo'],
            //'ccc_c' => $dataForm['num_banc'],
        );

        $createOperation = $this->createOperation($params);
        
        if(!empty($createOperation['id']))
        {
            $createRelated = false;
            $createRelated = $this->createRelatedOperation($createOperation['id'], $idInvoice, $clientProvider['id']);
            $params = array(
                'estado_c' => 'solicitud_anticipo',
            );

            $updateInvoice = $this->updateRecord($idInvoice, $params);

            if($createRelated && $updateInvoice){
                return new ViewModel(array(
                    'client' => $client,
                    'isProvider' => $this->_isProvider,
                    'isClient' => $this->_isClient,
                ));
            }else{
                return $this->redirect()->toRoute('error');
            }
        }
    }

    public function submitAddAction()
    {
        $idClient = $this->params()->fromRoute('id', -1);
        $client = $this->getClientbyId($idClient);
        if($this->getRequest()->isPost()) {      
            $dataForm = $this->params()->fromPost();
            
            $form = new ProviderForm();
            $form->setData($dataForm);
            if(!$form->isValid()) {
                $errors = $form->getMessages();
                $this->flashMessenger()->setNamespace('error');
                foreach($errors as $field => $error){
                    $this->flashMessenger()->addMessage('El campo ' . $form->get($field)->getLabel() . ' es requerido');
                }

                return $this->redirect()->toRoute('finance', [
                    'action' => 'addfactura', 
                    'id' => $idClient
                ]);
            }

            $file = $this->getRequest()->getFiles()->toArray();
            $uploadDir = ROOT_PATH . '/../uploads/';
            $result = move_uploaded_file($file['file_invoice']['tmp_name'], $uploadDir . $file["file_invoice"]["name"]);
            if($result){
                @unlink($file['file_invoice']['tmp_name']);
            }

            //Provider
            $params = array(
                'id' => $dataForm['idProveedor'],
                'name' => $dataForm['name'],
                'cif_c' => $dataForm['cif_c'],
                'email1' => $dataForm['email'],
                'billing_address_street' => $dataForm['billing_address_street'],   
                'billing_address_city' => $dataForm['billing_address_city'],
                'billing_address_state' => $dataForm['billing_address_state'],
                'billing_address_postalcode' => $dataForm['billing_address_postalcode'],
                'billing_address_country' => $dataForm['billing_address_country']

            );
            $provider = $this->insertOrUpdateProvider($params);

            //Relate Provider and Client 
            if(!empty($provider['id'])){
                $this->sugar->relateRecord('Accounts', $client['id'], 'members', $provider['id'])->execute();
            }

            $contentFile = file_get_contents($uploadDir . $file["file_invoice"]["name"]);
            $fecha_factura_c = date("Y-m-d", strtotime($dataForm['fecha_factura_c']));
            $fecha_vencimiento_c = date("Y-m-d", strtotime($dataForm['fecha_vencimiento_c']));
            $params = array(
                'name' => 'Factura Portal',
                'description' => $dataForm['description'],
                'estado_c' => '',
                'fecha_factura_c' => $fecha_factura_c,
                'fecha_vencimiento_c' => $fecha_vencimiento_c,
                'id_factura' => $dataForm['id_factura'],
                'importe_c' => $dataForm['importe_c'],
            );
            $createInvoice = $this->createInvoice($params);

            $createRelated = false;
            if(!empty($createInvoice['id']))
            {
                $createRelated = $this->createRelatedInvoice($createInvoice['id'], $dataForm['idClient'], $provider['id']);           
                $this->sugar->updateRecord('fac_facturas', $createInvoice['id'])->execute([
                    'estado_c' => 'disponible',
                ]);

                if($params['id'] == 'addprovider'){
                    //Enviamos el Mail de Bienvenida
                    $this->sugar->updateRecord('Accounts', $provider['id'])->execute([
                        'mail_welcome_provider_c' => '1',
                    ]);
                }
            }

            //Create Document
            if($createRelated)
            {
                $params = array(
                    'document_name' => $file['file_invoice']['name'],
                    'template_type' => 'invoice',
                );
                $createDocument = $this->createDocument($params);
                $createRelated = $this->createRelatedDocument($createDocument['id'], $createInvoice['id']);
                //Añadir documento al Registro Document
                $addDocumment = $this->addDocumment($createDocument['id'], $uploadDir . $file['file_invoice']['name']);
            }
            if($createRelated && $createDocument){
                return new ViewModel(array(
                    'client' => $client,
                    'isProvider' => $this->_isProvider,
                    'isClient' => $this->_isClient,
                ));
            }else{
                return $this->redirect()->toRoute('error');
            }
        }
        return $this->redirect()->toRoute('finance', ['action'=>'addfactura', 'id'=>$idClient]);
    }

    public function successProviderAction()
    {
        $emailUser = $this->authService->getIdentity();
        $client = $this->getClientbyEmail($emailUser);
        return new ViewModel(array(
            'client' => $client,
            'isProvider' => $this->_isProvider,
            'isClient' => $this->_isClient,
        ));
    }

    public function downloadAction()
    {
        $idInvoice = $this->params()->fromRoute('id');
        //Check ownership
        $hasPermissions = $this->_hasPermissionsForRecord($idInvoice);
        if(!$hasPermissions){
            return $this->redirect()->toRoute('no_access');
            exit;
        }

        $emailUser = $this->authService->getIdentity();
        $client = $this->getClientbyEmail($emailUser);
        $document = $this->getDocumentInvoice($idInvoice);
        if(!empty($document))
        {
            $response = $this->getResponse();
            $headers  = $response->getHeaders();
            $nameDoc = $document['filename'];
            $headers->addHeaderLine('Content-Type', 'application/octet-stream')
                ->addHeaderLine('Content-Disposition', "attachment; filename=$nameDoc")
                ->addHeaderLine("Pragma: no-cache")
                ->addHeaderLine("Content-Transfer-Encoding: UTF-8");

            $response->setContent($document['content']);
            return $response;
        }

    }

    public function calculateAction()
    {
        $idInvoice = $this->params()->fromRoute('idInvoice', -1);
        $params = $this->params()->fromPost();
        $emailUser = $this->authService->getIdentity();
        $client = $this->getClientbyEmail($emailUser);
        $infoInvoice = $this->getInvoice($params['idInvoice']);
        $clientClient = $this->getClientbyId($infoInvoice['accounts_fac_facturas_1accounts_ida']);
        //echo '<pre>importe_c: '.print_r($params['importe_c'], TRUE).'</pre>';
        //echo '<pre>fecha_vencimiento_real_c: '.print_r($params['fecha_vencimiento_real_c'], TRUE).'</pre>';
        //echo '<pre>new_fecha_vencimiento_c: '.print_r($params['new_fecha_vencimiento_c'], TRUE).'</pre>';
        //echo '<pre>clientClient: '.print_r($clientClient, TRUE).'</pre>';
        $matrizPor = $this->conditions($params['importe_c'], $params['fecha_vencimiento_real_c'], $params['new_fecha_vencimiento_c'], $clientClient);
        //echo '<pre>matrizPor:'.print_r($matrizPor, TRUE).'</pre>';
        //calculo
        $TIN = '4.95';
        $dateNow = new \DateTime($params['fecha_vencimiento_real_c']);
        $dateVencimiento = new \DateTime($params['new_fecha_vencimiento_c']);
        $interval = $dateNow->diff($dateVencimiento);
        $diasInterval = $interval->format('%a');

        $importeTIN = round((($params['importe_c'] * ($TIN/100))/365),2,PHP_ROUND_HALF_UP)*$diasInterval;
        $numComision = $matrizPor - $TIN;
        $comAbertura = round((($params['importe_c'] * ($numComision/100))/365),2,PHP_ROUND_HALF_UP)*$diasInterval;


        $totalComision = $importeTIN + $comAbertura;
        $retencion = ($totalComision*19)/100; //19% Retención
        $impTotalLiquidar = $totalComision - $retencion;

        $data = array(
            'impTIN' => number_format(round($importeTIN,2,PHP_ROUND_HALF_UP),2,",","."),
            'comAbertura' => number_format(round($comAbertura,2,PHP_ROUND_HALF_UP),2,",","."),
            'retencion' => number_format(round($retencion,2,PHP_ROUND_HALF_UP),2,",","."),
            'impTotalLiquidar' => number_format(round($impTotalLiquidar,2,PHP_ROUND_HALF_UP),2,",","."),
            'totalComision' => number_format(round($totalComision,2,PHP_ROUND_HALF_UP),2,",","."),
            'porComision' => number_format(round($numComision,2,PHP_ROUND_HALF_UP),2,",","."),
        );

        echo json_encode($data);
        exit;
    }

    public function providerAction()
    {
        $idProvider = $this->params()->fromRoute('id', -1);
        $data = $this->getProviderById($idProvider);
        echo json_encode($data);
        exit;
    }

    public function languageAction()
    {
        $idLanguage = $this->params()->fromRoute('id', -1);
        
        $storage = $this->session->getStorage();
        $storage->__set('current_language', $idLanguage);

        if(!empty($storage->__get('current_language'))){
            return true;
        }else{
            return false;
        }
    }

    private function addDocumment($idDocumment = '', $contentFile = '')
    {
        $documment = false;
        if(!empty($idDocumment) && !empty($contentFile))
        {
            $FilterEndpoint = $this->sugar->attachFile('Documents', $idDocumment, 'filename');
            $response = $FilterEndpoint->execute($contentFile)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $documment;
        }
    }

    private function updateRecord($idInvoice = '', $params)
    {
        $updateInvoice = false;
        if(!empty($idInvoice) && !empty($params))
        {
            $FilterEndpoint = $this->sugar->updateRecord('fac_facturas', $idInvoice);
            $response = $FilterEndpoint->execute($params)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
                $updateInvoice = true;
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $updateInvoice;
        }
    }

    private function getDocumentInvoice($idInvoice = '')
    {
        if(!empty($idInvoice))
        {
            $FilterEndpoint = $this->sugar->filterRelated('fac_facturas', $idInvoice, 'fac_facturas_documents_1');
            $data = array(
                'max_num' => 1,
                'filter' => array(
                    array(
                        '$and' => array(
                            array(
                                //name starts with 'a'
                                "template_type" => array(
                                    '$equals' => 'invoice',
                                )
                            )
                        ),
                    ),
                ),
                'fields' => array("name", "filename")
            );
            $response = $FilterEndpoint->execute($data)->getResponse();

            if ($response->getStatus()=='200'){
                $datosDocument = $response->getBody();
                $FilterEndpoint = $this->sugar->getAttachment('Documents', $datosDocument['records'][0]['id'], 'filename');
                $response = $FilterEndpoint->execute()->getResponse();

                if ($response->getStatus()=='200'){
                    $datos = $response->getBody();
                    return $return = array(
                        "content" => $datos,
                        "filename" => $datosDocument['records'][0]['filename'],
                    );
                }else{
                    if ($response->getError() === FALSE){
                        //echo $response->getBody();
                    }else{
                       //echo $response->getError();
                    }
                }
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
        }else{
            return false;
        }
    }

    private function createRelatedOperation($idOperation = '', $idInvoice = '', $idProvider = '')
    {
        $allRelatedOk = false;
        if(!empty($idOperation) && !empty($idInvoice) && !empty($idProvider))
        {
            //Related  Operation to Invoice
            $FilterEndpoint = $this->sugar->relateRecord('oper_Operaciones', $idOperation, 'oper_operaciones_fac_facturas_1', $idInvoice);
            $response = $FilterEndpoint->execute()->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();

                //Related  Operation to Client
                $FilterEndpoint = $this->sugar->relateRecord('oper_Operaciones', $idOperation, 'accounts_oper_operaciones_1', $idProvider);
                $response = $FilterEndpoint->execute()->getResponse();
                if ($response->getStatus()=='200'){
                    $datos = $response->getBody();
                    $allRelatedOk = true;
                }else{
                    if ($response->getError() === FALSE){
                        //echo $response->getBody();
                    }else{
                       //echo $response->getError();
                    }
                }
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $allRelatedOk;
        }else{
            return $allRelatedOk;
        }
    }

    private function createRelatedInvoice($idInvoice = '', $idClient = '', $idProveedor = '')
    {
        $allRelatedOk = false;
        if(!empty($idInvoice) && !empty($idClient) && !empty($idProveedor))
        {
            //Related  Operation to Invoice
            $FilterEndpoint = $this->sugar->relateRecord('fac_facturas', $idInvoice, 'accounts_fac_facturas_1', $idClient);
            $response = $FilterEndpoint->execute()->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();

                //Related  Operation to Client
                $FilterEndpoint = $this->sugar->relateRecord('fac_facturas', $idInvoice, 'accounts_fac_facturas_2', $idProveedor);
                $response = $FilterEndpoint->execute()->getResponse();
                if ($response->getStatus()=='200'){
                    $datos = $response->getBody();
                    $allRelatedOk = true;
                }else{
                    if ($response->getError() === FALSE){
                        //echo $response->getBody();
                    }else{
                       //echo $response->getError();
                    }
                }
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $allRelatedOk;
        }else{
            return $allRelatedOk;
        }
    }

    private function createRelatedDocument($idDocument = '', $idInvoice = '')
    {
        $allRelatedOk = false;
        if(!empty($idDocument) && !empty($idInvoice))
        {
            //Related  Operation to Invoice
            $FilterEndpoint = $this->sugar->relateRecord('fac_facturas', $idInvoice, 'fac_facturas_documents_1', $idDocument);
            $response = $FilterEndpoint->execute()->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
                $allRelatedOk = true;
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $allRelatedOk;
        }else{
            return $allRelatedOk;
        }
    }

    private function createOperation($params = '')
    {
        $operation = '';
        if(!empty($params))
        {
            $FilterEndpoint = $this->sugar->createRecord('oper_Operaciones');
            $response = $FilterEndpoint->execute($params)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $operation;
        }
    }

    private function createInvoice($params = '')
    {
        $invoice = '';
        if(!empty($params))
        {
            $FilterEndpoint = $this->sugar->createRecord('fac_facturas');
            $response = $FilterEndpoint->execute($params)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $invoice;
        }
    }

    private function createContact($params = '')
    {
        $contact = '';
        if(!empty($params))
        {
            //echo '<pre>'.print_r($params, TRUE).'</pre>';
            $FilterEndpoint = $this->sugar->createRecord('Contacts');
            $response = $FilterEndpoint->execute($params)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $contact;
        }
    }

    private function insertOrUpdateProvider($params = array())
    {
        $provider = '';
        if(!empty($params))
        {
            if(isset($params['id']) && $params['id'] != 'addprovider'){
                $FilterEndpoint = $this->sugar->updateRecord('Accounts', $params['id']);
                unset($params['id']);
                unset($params['phone_office']);
                unset($params['contact_name']);
            }else{
                unset($params['id']);
                unset($params['contact_name']);
                $params['estado_c'] = '1';
                $params['account_type'][] = '2';
                $FilterEndpoint = $this->sugar->createRecord('Accounts');
            }
            $response = $FilterEndpoint->execute($params)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $provider;
        }
    }

    private function createDocument($params = '')
    {
        $document = '';
        if(!empty($params))
        {
            $FilterEndpoint = $this->sugar->createRecord('Documents');
            $response = $FilterEndpoint->execute($params)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $document;
        }
    }

    private function getInvoice($idInvoice = '')
    {
        $infoInvoice = '';
        if(!empty($idInvoice))
        {
            $FilterEndpoint = $this->sugar->getRecord('fac_facturas', $idInvoice);
            $response = $FilterEndpoint->execute()->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $infoInvoice;
        }
    }

    private function getInvoices($idClient = '')
    {
        if(!empty($idClient))
        {
            $FilterEndpoint = $this->sugar->filterRelated('Accounts', $idClient, 'accounts_fac_facturas_2');
            $data = array(
                'filter' => array(
                    array(
                        'estado_c' => array(
                            '$not_in' => array('anticipada','liquidada','eliminada'),
                        ),
                    )
                ),
                'max_num' => 999,
                'order_by' => 'fecha_vencimiento_c:asc',
            );
            $response = $FilterEndpoint->execute($data)->getResponse();

            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $datos;
        }else{
            return false;
        }
    }

    private function getInvoicesHistoric($idClient = '')
    {
        if(!empty($idClient))
        {
            $FilterEndpoint = $this->sugar->filterRelated('Accounts', $idClient, 'accounts_fac_facturas_2');
            $data = array(
                 'filter' => array(
                    array(
                        'estado_c' => array(
                            '$in' => array('anticipada','liquidada','eliminada'),
                        ),
                    )
                ),
                'max_num' => 999,
                'order_by' => 'fecha_vencimiento_c:asc',
            );
            $response = $FilterEndpoint->execute($data)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $datos;
        }else{
            return false;
        }
    }

    private function getInvoicesHistoricCliente($idClient = '')
    {
        if(!empty($idClient))
        {
            $FilterEndpoint = $this->sugar->filterRelated('Accounts', $idClient, 'accounts_fac_facturas_1');
            $data = array(
                'filter' => array(
                    array(
                        'estado_cliente_c' => array(
                            '$in' => array('liquidada'),
                        ),
                    ),                    
                ),
                'max_num' => 999,
                'order_by' => 'fecha_vencimiento_real_c:asc',
            );
            $response = $FilterEndpoint->execute($data)->getResponse();
            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $datos;
        }else{
            return false;
        }
    }

    private function getInvoicesClient($idClient = '')
    {
        if(!empty($idClient))
        {
            $FilterEndpoint = $this->sugar->filterRelated('Accounts', $idClient, 'accounts_fac_facturas_1');
            $fecha = date('Y-m-d');
            $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
            $data = array(
                'filter' => array(
                    array(
                        '$and' => array(
                            array(
                                'estado_cliente_c' => array(
                                    '$not_in' => array('liquidada'),
                                ),
                                'estado_c' => array(
                                    '$not_in' => array('eliminada'),
                                ),
                            ),
                        ),
                    ),
                ),
                'max_num' => 999,
                'order_by' => 'fecha_vencimiento_real_c:asc',
            );
            $response = $FilterEndpoint->execute($data)->getResponse();

            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $datos;
        }else{
            return false;
        }
    }

    private function getInvoicesClientCredito($idClient = '')
    {
        if(!empty($idClient))
        {
            $FilterEndpoint = $this->sugar->filterRelated('Accounts', $idClient, 'accounts_fac_facturas_1');
            $data = array(
                'filter' => array(
                    array(
                        '$and' => array(
                            array(
                                'estado_cliente_c' => array(
                                    '$not_in' => array('liquidada'),
                                ),
                                '$and' => array(
                                    array(
                                        'estado_c' => array(
                                            '$not_in' => array('eliminada'),
                                        ),
                                        'estado_c' => array(
                                            '$in' => array('liquidada','anticipada'),
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
                'max_num' => 999,
            );
            $response = $FilterEndpoint->execute($data)->getResponse();

            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $datos;
        }else{
            return false;
        }
    }

    private function getProvidersByClientId($idClient = '')
    {
        if(!empty($idClient))
        {
            $FilterEndpoint = $this->sugar->filterRelated('Accounts', $idClient, 'members');
            $data = array(
                'max_num' => 999,
            );
            $response = $FilterEndpoint->execute($data)->getResponse();

            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return $datos;
        }else{
            return false;
        }
    }

    private function getProviderById($idProvider = '')
    {
        $infoProvider = '';
        if(!empty($idProvider))
        {
            $FilterEndpoint = $this->sugar->getRecord('Accounts', $idProvider);
            $response = $FilterEndpoint->execute()->getResponse();
            if ($response->getStatus() == '200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return $infoProvider;
        }
    }


    private function getClientbyEmail($email = '')

    {
        if(!empty($email))
        {   
            $FilterEndpoint = $this->sugar->filterRecords('Accounts');
            $data = array(
                'max_num' => 1,
                'filter' => array(
                    array(
                        '$or' => array(
                            array(
                                //name starts with 'a'
                                "email1" => array(
                                    '$starts' => $email,
                                )
                            )
                        ),
                    ),
                ),
                'fields' => array("name", "cif_c","iban_c","account_type","tipo_interes_c","credito_c")
            );
            $response = $FilterEndpoint->execute($data)->getResponse();

            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos['records'][0])) ? $datos['records'][0] : '';
        }else{
            return null;
        }
    }

    private function getClientbyId($idClient = '')
    {
        if(!empty($idClient))
        {   
            $FilterEndpoint = $this->sugar->getRecord('Accounts', $idClient);
            $response = $FilterEndpoint->execute()->getResponse();

            if ($response->getStatus()=='200'){
                $datos = $response->getBody();
            }else{
                if ($response->getError() === FALSE){
                    //echo $response->getBody();
                }else{
                   //echo $response->getError();
                }
            }
            return (!empty($datos)) ? $datos : '';
        }else{
            return null;
        }
    }

    private function conditions($importInvoice, $dateFrom, $dateVencimiento, $client)
    {
        $porcentaje = 0;
        if(!empty($importInvoice))
        {
           // echo '<pre>tipo_interes_c: '.print_r($client['tipo_interes_c'], TRUE).'</pre>';
            $matrizContions = $this->_selectMatrizConditions($client['tipo_interes_c']);
           // echo '<pre>matrizContions: '.print_r($matrizContions, TRUE).'</pre>';
            $dateNow = new \DateTime($dateFrom);
            $dateVencimiento = new \DateTime($dateVencimiento);
            $interval = $dateNow->diff($dateVencimiento);
            //echo '<pre>'.print_r($dateNow, TRUE).'</pre>';
            //echo '<pre>'.print_r($dateVencimiento, TRUE).'</pre>';
            $diasInterval = $interval->format('%a');
            //echo '<pre>'.print_r($diasInterval, TRUE).'</pre>';
            //echo '<pre>'.print_r($importInvoice, TRUE).'</pre>';
            foreach ($matrizContions as $key => $value) {                
                //echo '<pre>ENTRA: '.print_r($value['max'], TRUE).'</pre>';
                if($value['max'] >= $importInvoice && $porcentaje == 0){

                    foreach ($value['days'] as $days => $por) {
                        //echo '<pre>DAYS: '.print_r($days, TRUE).'</pre>';
                        //echo '<pre>INTERVAL: '.print_r($diasInterval, TRUE).'</pre>';

                        if($days >= $diasInterval){
                            $porcentaje = $por;
                            break;
                        }else{
                            $porcentaje = $por;
                        }
                    }
                }
            }
            //echo '<pre>'.print_r($porcentaje, TRUE).'</pre>';

        }
        return $porcentaje;
    }

    private function _totalImportInvoices($invoices)
    {
        if(empty($invoices))
            return false;

        $sumImport = 0;

        foreach ($invoices['records'] as $invoice) {
            $sumImport = $sumImport + $invoice['importe_c'];
        }

        return $sumImport;
    }

    private function _hasPermissionsForRecord($invoiceId = '')
    {
        if(empty($invoiceId))
            return false;

        $identity = $this->authService->getIdentity();
        $client = $this->getClientbyEmail($identity);

        $response = $this->sugar->getRecord('fac_facturas', $invoiceId)->execute()->getResponse();
        if($response->getStatus() == '200'){
            $invoice = $response->getBody();
            if($invoice['accounts_fac_facturas_2accounts_ida'] != $client['id'] && $invoice['accounts_fac_facturas_1accounts_ida'] != $client['id']){
                return false;
            }
        }else{
            return false;
        }

        return true;
    }

    private function _selectMatrizConditions($tipo_interes)
    {
        switch ($tipo_interes) {
            case 'estandard':
                $matrizContions = array(
                    array(
                        'max' => 500,
                        'days' => array(
                            30 => 60,
                            60 => 60,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 1000,
                        'days' => array(
                            30 => 60,
                            60 => 60,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 2000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 3000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 4000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 5000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 6000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 7000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 8000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 9000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 10000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 15000,
                        'days' => array(
                            30 => 40,
                            60 => 40,
                            90 => 40,
                            'mas' => 30,
                        ),
                    ),
                    array(
                        'max' => 20000,
                        'days' => array(
                            30 => 30,
                            60 => 30,
                            90 => 30,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 9999999999,
                        'days' => array(
                            30 => 30,
                            60 => 30,
                            90 => 30,
                            'mas' => 25,
                        ),
                    ),
                );
                break;
            case 'vip':
                $matrizContions = array(
                    array(
                        'max' => 500,
                        'days' => array(
                            30 => 30,
                            60 => 30,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 1000,
                        'days' => array(
                            30 => 30,
                            60 => 30,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 2000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 3000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 4000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 5000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 6000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 7000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 8000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 9000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 10000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 15000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 20000,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 9999999999,
                        'days' => array(
                            30 => 25,
                            60 => 25,
                            90 => 25,
                            'mas' => 25,
                        ),
                    ),
                );
                break;
            case 'alto_riesgo':
                $matrizContions = array(
                    array(
                        'max' => 500,
                        'days' => array(
                            30 => 80,
                            60 => 80,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 1000,
                        'days' => array(
                            30 => 80,
                            60 => 80,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 2000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 3000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 4000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 5000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 6000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 7000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 8000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 9000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 10000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 15000,
                        'days' => array(
                            30 => 70,
                            60 => 70,
                            90 => 70,
                            'mas' => 60,
                        ),
                    ),
                    array(
                        'max' => 20000,
                        'days' => array(
                            30 => 60,
                            60 => 60,
                            90 => 60,
                            'mas' => 50,
                        ),
                    ),
                    array(
                        'max' => 9999999999,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 45,
                        ),
                    ),
                );
                break;
            default:
                $matrizContions = array(
                    array(
                        'max' => 500,
                        'days' => array(
                            30 => 60,
                            60 => 60,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 1000,
                        'days' => array(
                            30 => 60,
                            60 => 60,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 2000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 3000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 4000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 5000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 6000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 7000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 8000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 9000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 10000,
                        'days' => array(
                            30 => 50,
                            60 => 50,
                            90 => 50,
                            'mas' => 40,
                        ),
                    ),
                    array(
                        'max' => 15000,
                        'days' => array(
                            30 => 40,
                            60 => 40,
                            90 => 40,
                            'mas' => 30,
                        ),
                    ),
                    array(
                        'max' => 20000,
                        'days' => array(
                            30 => 30,
                            60 => 30,
                            90 => 30,
                            'mas' => 25,
                        ),
                    ),
                    array(
                        'max' => 9999999999,
                        'days' => array(
                            30 => 30,
                            60 => 30,
                            90 => 30,
                            'mas' => 25,
                        ),
                    ),
                );
                break;
        }
        return $matrizContions;           
    }
}

