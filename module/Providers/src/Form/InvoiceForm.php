<?php
namespace Providers\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;
use Providers\Validator\DateGreaterThanValidator;

class InvoiceForm extends Form 
{
	private $request = null;

	public function __construct($request = null)
	{
		// Define form name
        parent::__construct('invoice-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        
        $this->request = $request;

        $this->addElements();
        $this->addInputFilter();
	}

	/**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        // Add "importe_c" field
        $this->add([            
            'type'  => 'text',
            'name' => 'importe_c',
            'options' => [
                'label' => 'Importe',
            ],
        ]);

        // Add "id_factura" field
        $this->add([            
            'type'  => 'text',
            'name' => 'id_factura',
            'options' => [
                'label' => 'Núm. Factura',
            ],
        ]);

        // Add "fecha_factura_c" field

        $this->add([
		    'type' => 'date',
		    'name' => 'fecha_factura_c',
		    'options' => [
		        'label' => 'Fecha de Factura',
		        'format' => 'd-m-Y',
		    ],
		]);

        // Add "fecha_vencimiento_c" field

        $this->add([
		    'type' => 'date',
		    'name' => 'fecha_vencimiento_c',
		    'options' => [
		        'label' => 'Fecha Vencimiento',
		        'format' => 'd-m-Y',
		    ],
		]);

        // Add the Submit button "Enviar Factura"
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [                
                'value' => 'Enviar Factura'
            ],
        ]);
    }

    private function addInputFilter() 
    {
        $inputFilter = $this->getInputFilter();
        
        // Add "importe_c" field
        $inputFilter->add([            
            'name' => 'importe_c',
            'required' => true,
        ]);

        // Add "id_factura" field
        $inputFilter->add([            
            'name' => 'id_factura',
            'required' => true,
        ]);

        // Add "fecha_factura_c" field
        $inputFilter->add([
            'name' => 'fecha_factura_c',
            'required' => true,
        ]);

        // Add "fecha_vencimiento_c" field
        $inputFilter->add([
            'name' => 'fecha_vencimiento_c',
            'required' => true,
            'validators' => [
                [
                    'name' => DateGreaterThanValidator::class,
                    'options' => [
                        'field' => 'fecha_factura_c',
                        'request' => $this->request,
                    ]
                ]
            ]
        ]);
    }
}