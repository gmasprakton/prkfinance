<?php
namespace Providers\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;
use User\Validator\UserExistsValidator;

class LiquidationForm extends Form
{
    private $isAplaza = false;
   	/**
    * Constructor.     
    */
    public function __construct($isAplaza = false)
    {
        // Define form name
        parent::__construct('liquidation-form');
        
        $this->isAplaza = $isAplaza;

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        
        $this->addElements();
        $this->addInputFilter();          
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
    	// Add "conditions" field
        $this->add([            
            'type'  => 'checkbox',
            'name' => 'conditions',            
            'options' => [
                'label' => 'Condiciones de contrato',
            ],
        ]);

        //Add "iban_c" field
        $this->add([            
            'type'  => 'text',
            'name' => 'iban_c',            
            'options' => [
                'label' => 'IBAN',
            ],
        ]);

        $this->add([
            'type' => 'date',
            'name' => 'new_fecha_vencimiento_c',
            'options' => [
                'label' => 'Fecha Vencimiento',
                'format' => 'd-m-Y',
            ],
        ]);
    }

    private function addInputFilter() 
    {
        $inputFilter = $this->getInputFilter();

        // Add "conditions" field
        $inputFilter->add([            
            'name' => 'conditions',
            'required' => true,
        ]);

        $inputFilter->add([
            'name' => 'new_fecha_vencimiento_c',
            'required' => $this->isAplaza,
        ]);

        // Add "iban_c" field
        $inputFilter->add([            
            'name' => 'iban_c',
            'required' => !$this->isAplaza,
        ]);
    }
}