<?php
namespace Providers\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;
use Providers\Validator\DateGreaterThanValidator;

class ProviderForm extends Form
{
    private $request = null;
    private $newProvider = false;

   	/**
    * Constructor.     
    */
    public function __construct($request = null, $newProvider = false)
    {
        // Define form name
        parent::__construct('provider-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        
        $this->request = $request;
        $this->newProvider = $newProvider;

        $this->addElements();
        $this->addInputFilter();          
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {

    	// Add "idClient" field
    	$this->add([            
            'type'  => 'hidden',
            'name' => 'idClient',
            'options' => [
                'label' => 'Client - ID',
            ],
        ]);

    	// Add "idProveedor" field
        $this->add([            
            'type'  => 'text',
            'name' => 'idProveedor',            
            'options' => [
                'label' => 'ID Proveedor',
            ],
        ]);

		// Add "cif_c" field
        $this->add([            
            'type'  => 'text',
            'name' => 'cif_c',            
            'options' => [
                'label' => 'CIF',
            ],
        ]);

        // Add "cif_c" field
        $this->add([            
            'type'  => 'text',
            'name' => 'iban',            
            'options' => [
                'label' => 'IBAN',
            ],
        ]);

		// Add "name" field
		$this->add([            
            'type'  => 'text',
            'name' => 'name',            
            'options' => [
                'label' => 'Name',
            ],
        ]);

        // Add "email" field
        $this->add([            
            'type'  => 'text',
            'name' => 'email',
            'options' => [
                'label' => 'E-mail',
            ],
        ]);

        // Add "phone" field
        $this->add([            
            'type'  => 'text',
            'name' => 'phone',
            'options' => [
                'label' => 'Teléfono',
            ],
        ]);

        // Add "contact" field
        $this->add([            
            'type'  => 'text',
            'name' => 'contact',
            'options' => [
                'label' => 'Persona Contacto',
            ],
        ]);


        // Add "billing_address_street" field
        $this->add([            
            'type'  => 'textarea',
            'name' => 'billing_address_street',
            'options' => [
                'label' => 'Dirección',
            ],
        ]);

        // Add "billing_address_city" field
        $this->add([            
            'type'  => 'textarea',
            'name' => 'billing_address_city',
            'options' => [
                'label' => 'Ciudad',
            ],
        ]);

        // Add "billing_address_state" field
        $this->add([            
            'type'  => 'textarea',
            'name' => 'billing_address_state',
            'options' => [
                'label' => 'Estado/Provincia',
            ],
        ]);

        // Add "billing_address_postalcode" field
        $this->add([            
            'type'  => 'textarea',
            'name' => 'billing_address_postalcode',
            'options' => [
                'label' => 'Código Postal',
            ],
        ]);
        // Add "billing_address_country" field
        $this->add([            
            'type'  => 'textarea',
            'name' => 'billing_address_country',
            'options' => [
                'label' => 'Pais',
            ],
        ]);
        
        // Add "importe_c" field
        $this->add([            
            'type'  => 'text',
            'name' => 'importe_c',
            'options' => [
                'label' => 'Importe',
            ],
        ]);

        // Add "id_factura" field
        $this->add([            
            'type'  => 'text',
            'name' => 'id_factura',
            'options' => [
                'label' => 'Núm. Factura',
            ],
        ]);

        // Add "fecha_factura_c" field

        $this->add([
		    'type' => 'date',
		    'name' => 'fecha_factura_c',
		    'options' => [
		        'label' => 'Fecha de Factura',
		        'format' => 'd-m-Y',
		    ],
		]);

        // Add "fecha_vencimiento_c" field

        $this->add([
		    'type' => 'date',
		    'name' => 'fecha_vencimiento_c',
		    'options' => [
		        'label' => 'Fecha Vencimiento',
		        'format' => 'd-m-Y',
		    ],
		]);

        // Add "description" field
        $this->add([            
            'type'  => 'textarea',
            'name' => 'description',
            'options' => [
                'label' => 'Detalles adicionales',
            ],
        ]);

         // Add "file_invoice" field
        $this->add([            
            'type'  => 'file',
            'name' => 'file_invoice',
            'options' => [
                'label' => 'Adjuntar archivo',
            ],
        ]);
        
        // Add the Submit button "Enviar Factura"
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [                
                'value' => 'Enviar Factura'
            ],
        ]);
    }

    private function addInputFilter() 
    {
        $inputFilter = $this->getInputFilter();

        // Add "idClient" field
        $inputFilter->add([            
            'name' => 'idClient',
            'required' => true,
        ]);

        // Add "idProveedor" field
        $inputFilter->add([            
            'name' => 'idProveedor',            
            'required' => true,
        ]);

        // Add "cif_c" field
        $inputFilter->add([            
            'name' => 'cif_c',            
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

        // Add "iban" field
        $inputFilter->add([            
            'name' => 'iban',            
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

        // Add "name" field
        $inputFilter->add([            
            'name' => 'name',            
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

        // Add "iban" field
        $inputFilter->add([            
            'name' => 'phone',            
            'required' => $this->newProvider,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

        // Add "iban" field
        $inputFilter->add([            
            'name' => 'contact',            
            'required' => $this->newProvider,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
        ]);

        // Add "email" field
        $inputFilter->add([            
            'name' => 'email',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 128
                    ],
                ],
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                        'useMxCheck'    => false,                            
                    ],
                ],
            ],
        ]);

        // Add "billing_address_street" field
        $inputFilter->add([            
            'name' => 'billing_address_street',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],                    
            ],
        ]);

        // Add "billing_address_city" field
        $inputFilter->add([            
            'name' => 'billing_address_city',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],                    
            ],
        ]);

        // Add "billing_address_state" field
        $inputFilter->add([            
            'name' => 'billing_address_state',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],                    
            ],
        ]);

        // Add "billing_address_postalcode" field
        $inputFilter->add([            
            'name' => 'billing_address_postalcode',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],                    
            ],
        ]);

         // Add "billing_address_country" field
        $inputFilter->add([            
            'name' => 'billing_address_country',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],                    
            ],
        ]);
        
        // Add "importe_c" field
        $inputFilter->add([            
            'name' => 'importe_c',
            'required' => true,
        ]);

        // Add "id_factura" field
        $inputFilter->add([            
            'name' => 'id_factura',
            'required' => true,
        ]);

        // Add "fecha_factura_c" field
        $inputFilter->add([
            'name' => 'fecha_factura_c',
            'required' => true,
        ]);

        // Add "fecha_vencimiento_c" field
        $inputFilter->add([
            'name' => 'fecha_vencimiento_c',
            'required' => true,
            'validators' => [
                [
                    'name' => DateGreaterThanValidator::class,
                    'options' => [
                        'field' => 'fecha_factura_c',
                        'request' => $this->request,
                    ]
                ]
            ]
        ]);

        // Add "description" field
        $inputFilter->add([            
            'name' => 'description',
            'required' => false,
        ]);

        // Add "file_invoice" field
        $inputFilter->add([            
            'name' => 'file_invoice',
            'required' => false,
        ]);
    }
}