<?php
namespace Providers\Service\Factory;

use Interop\Container\ContainerInterface;
use Providers\Service\ProviderManager;

/**
 * This is the factory class for UserManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class ProviderManagerFactory
{
    /**
     * This method creates the ProviderManager service and returns its instance. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {        
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $viewRenderer = $container->get('ViewRenderer');
        $config = $container->get('Config');
                        
        return new ProviderManager($entityManager, $viewRenderer, $config);
    }
}
