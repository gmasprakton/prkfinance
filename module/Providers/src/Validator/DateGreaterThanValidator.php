<?php
namespace Providers\Validator;

use Zend\Validator\AbstractValidator;

class DateGreaterThanValidator extends AbstractValidator 
{
	const DATE2_GREATER_THAN_DATE1 = 'isLessThan';

	// Available validator options.
	protected $options = [
		'field' => '',
		'request' => null,
	];

	protected $messageTemplates = [
		self::DATE2_GREATER_THAN_DATE1 => "La fecha vencimiento no puede ser inferior a la fecha factura.",
	];

	public function __construct($options = null)
	{
		if(is_array($options)) {
			foreach($options as $option => $value){
				if(isset($this->options[$option])){
					$this->options[$option] = $value;
				}
			}
		}
		
		// Call the parent class constructor.
		parent::__construct($options);
	}

	public function isValid($value)
	{
		if(!empty($this->options['request'])){
			$request = $this->options['request'];
			$date = $this->options['request']->getPost()->get($this->options['field']);

			$date1 = \DateTime::createFromFormat('d-m-Y', $value);
			$date2 = \DateTime::createFromFormat('d-m-Y', $date);

			if($date1->getTimestamp() < $date2->getTimestamp()){
				$this->error(self::DATE2_GREATER_THAN_DATE1);
				return false;
			}
			return true;
		}
		return true;
	}
}
