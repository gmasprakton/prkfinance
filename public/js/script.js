 /*! Template: TokenWiz v1.0.1 */
(function($){
	'use strict';
	var $win = $(window), $body = $('body'), $doc = $(document);
	
	// Touch Class
	if (!("ontouchstart" in document.documentElement)) {
		$body.addClass("no-touch");
	}
	// Get Window Width
	function winwidth () {
		return $win.width();
	}
	var wwCurrent = winwidth();
	$win.on('resize', function () { 
		wwCurrent = winwidth(); 
	});

	var $is_sticky = $('.is-sticky'), $topbar = $('.topbar'), $topbar_wrap = $('.topbar-wrap');
	if ($is_sticky.length > 0 ) {
		var $navm = $is_sticky.offset();
		$win.scroll(function(){
			var $scroll = $win.scrollTop(), $topbar_height = $topbar.height();
			if($scroll > $navm.top){
                if(!$is_sticky.hasClass('has-fixed')) {$is_sticky.addClass('has-fixed');$topbar_wrap.css('padding-top', $topbar_height);}

            } else {
                if($is_sticky.hasClass('has-fixed')) {$is_sticky.removeClass('has-fixed');$topbar_wrap.css('padding-top', 0);}
            }
		});
	}
    
    //Data Percent
    var $data_percent = $('[data-percent]');
    if($data_percent.length > 0){
        $data_percent.each(function() {
            var $this = $(this), $this_percent = $this.data('percent');
            $this.css('width', $this_percent + '%');
        });
    }
    
	// Active page menu when click
	var CurURL = window.location.href, urlSplit = CurURL.split("#");
	var $nav_link = $("a");
	if ($nav_link.length > 0) {
		$nav_link.each(function() {
			if (CurURL === (this.href) && (urlSplit[1]!=="")) {
				$(this).closest("li").addClass("active").parent().closest("li").addClass("active");
			}
		});
	}
    
    // Countdown Clock
    var $count_token_clock = $('.countdown-clock');
	if ($count_token_clock.length > 0 ) {
		$count_token_clock.each(function() {
			var $self = $(this), datetime = $self.attr("data-date");
			$self.countdown(datetime).on('update.countdown', function(event) {
				$(this).html(event.strftime('<div><span class="countdown-time countdown-time-first">%D</span><span class="countdown-text">Day</span></div>' + '<div><span class="countdown-time">%H</span><span class="countdown-text">Hour</span></div>' + '<div><span class="countdown-time">%M</span><span class="countdown-text">Min</span></div>' + '<div><span class="countdown-time countdown-time-last">%S</span><span class="countdown-text">Sec</span></div>'));
			});
		});
	}
    
	// Select
	var $select = $('.select');
	if ($select.length > 0) {
        $select.each(function() {
			var $this = $(this);
            $this.select2({
                theme: "flat"
            });
		});
	}
	var $select_bdr = $('.select-bordered');
	if ($select_bdr.length > 0) {
        $select_bdr.each(function() {
			var $this = $(this);
            $this.select2({
                theme: "flat bordered"
            });
		});
	}

    // Toggle section On click
    var _trigger = '.toggle-tigger', _toggle = '.toggle-class';
    
    if ($(_trigger).length > 0 ) {
		$doc.on('click', _trigger, function(e){
            var $self = $(this);
            $(_trigger).not($self).removeClass('active');
            $(_toggle).not($self.parent().children()).removeClass('active');
            $self.toggleClass('active').parent().find(_toggle).toggleClass('active');
            e.preventDefault();
        });
	}
    
    $doc.on('click', 'body', function(e){
        var $elm_tig = $(_trigger), $elm_tog = $(_toggle);
		if (!$elm_tog.is(e.target) && $elm_tog.has(e.target).length===0 && 
            !$elm_tig.is(e.target) && $elm_tig.has(e.target).length===0) {
                $elm_tog.removeClass('active');
                $elm_tig.removeClass('active');
		}
	});
    
    // Mobile Nav
    var $toggle_nav = $('.toggle-nav'),  $navbar = $('.navbar');
    if($toggle_nav.length > 0){
        $toggle_nav.on('click', function(e){
            $toggle_nav.toggleClass('active');
            $navbar.toggleClass('active');
            e.preventDefault();
        });
    }
    $doc.on('click', 'body', function(e){
		if (!$toggle_nav.is(e.target) && $toggle_nav.has(e.target).length===0 && 
            !$navbar.is(e.target) && $navbar.has(e.target).length===0) {
            $toggle_nav.removeClass('active');
            $navbar.removeClass('active');
		}
	});
    
    function activeNav(navbar){
        if(wwCurrent < 991){
            navbar.delay(500).addClass('navbar-mobile');
        }else{
            navbar.delay(500).removeClass('navbar-mobile');
        }
    }
    activeNav($navbar);
    $win.on('resize', function () { 
        activeNav($navbar);
	});
    
    
    // Tooltip
    var $tooltip = $('[data-toggle="tooltip"]');
    if($tooltip.length > 0){
        $tooltip.tooltip();
    }
    
    // Date Picker
    var $date_picker = $('.date-picker'), $date_picker_dob = $('.date-picker-dob'), $time_picker = $('.time-picker');
    if($date_picker.length > 0){
        $date_picker.each(function(){
            $(this).datepicker({
                format: "dd-mm-yyyy",
                maxViewMode: 2,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                weekStart: 1
            });
        });
    }
    if($date_picker_dob.length > 0){
        $date_picker_dob.each(function(){
            $(this).datepicker({
                format: "dd-mm-yyyy",
                startView: 2,
                maxViewMode: 2,
                clearBtn: true,
                autoclose: true
            });
        });
    }
    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
        daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Hoy"
    };
    
    // Time Picker
    if($time_picker.length > 0){
        $time_picker.each(function(){
            $(this).parent().addClass('has-timepicker');
            $(this).timepicker({
                timeFormat: 'HH:mm',
                interval: 15
            });
        });
    }
    
    
    //Copy Text to Clipboard
    function copytoclipboard(triger,action,feedback){
        var supportCopy = document.queryCommandSupported('copy'), $triger = triger, $action = action, $feedback = feedback;
        
        $triger.parent().find($action).removeAttr('disabled').select();
        if (supportCopy===true) {
            
            document.execCommand("copy");
            $feedback.text('Copied to Clipboard').fadeIn().delay(1000).fadeOut();
            $triger.parent().find($action).attr('disabled', 'disabled');
        } else {
            window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
        }

    }
    
    // Copyto clipboard Feedback Function
    function feedback (el, state) {
        if (state==='success'){
            $(el).parent().find('.copy-feedback').text('Copied to Clipboard').fadeIn().delay(1000).fadeOut();
        } else {
            $(el).parent().find('.copy-feedback').text('Faild to Copy').fadeIn().delay(1000).fadeOut();
        }
    }
    // Copyto clipboard 
    var clipboard = new ClipboardJS('.copy-clipboard');
    clipboard.on('success', function(e) {
        feedback(e.trigger, 'success'); e.clearSelection();
    }).on('error', function(e) {
        feedback(e.trigger, 'fail');
    });
    
    // Copyto clipboard In Modal
    var clipboardModal = new ClipboardJS('.copy-clipboard-modal', {
        container: document.querySelector('.modal')
    });
    clipboardModal.on('success', function(e) {
        feedback(e.trigger, 'success'); e.clearSelection();
    }).on('error', function(e) {
        feedback(e.trigger, 'fail');
    });
    
    
    //File Input
    var $input_file = $('.input-file');
    if($input_file.length > 0){
        $input_file.each(function(){
            var $this = $(this), $thislabel = $(this).next(), $thislabeltext = $(this).next().text();
            $this.on('change', function(){
                var $thisval = $this.val();
                $thislabel.html($thisval);
                if ($thislabel.is(':empty')){
                    $thislabel.html($thislabeltext);
                }
            });
        });
    }
    
    // Dropzone
	var $upload_zone = $('.upload-zone');
	if ($upload_zone.length > 0 ) {
        Dropzone.autoDiscover = false;
		$upload_zone.each(function(){
			var $self = $(this);
			$self.addClass('dropzone').dropzone({ url: "/images" });
		});
	}

    //magnificPopup	Content
	var $image_popup = $('.image-popup');
	if ($image_popup.length > 0 ) {
		$image_popup.magnificPopup({
			type: 'image',
			preloader: true,
			removalDelay: 400,
			mainClass: 'mfp-fade'
		});
	}
    
    // Data Tables
    var $data_table = $('.dt-init');
    if($data_table.length > 0){
       $data_table.DataTable({
           "ordering": false,
           "autoWidth": false,
           "dom":'<t><"row align-items-center"<"col-sm-6 text-left"p><"col-sm-6 text-sm-right"i>>',
           "pageLength": 10, 
           "bPaginate" : $('.data-table tbody tr').length>5,
           "iDisplayLength": 10,
           "language": {
                "search": "",
                "searchPlaceholder": "Escribe para buscar",
                "info": "_START_ -_END_ de _TOTAL_",
                "infoEmpty": "Sin datos",
                "infoFiltered": "( Total _MAX_  )",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
            },
        });
    }
    
    
    var $data_table_filter = $('.dt-filter-init');
    if($data_table_filter.length > 0){
       var $data_table_fltr = $data_table_filter.DataTable({
           "ordering": false,
           autoWidth: false,
           "dom":'<"row justify-content-between pdb-1x"<"col-9 col-sm-6 text-left"f><"col-3 text-right"<"data-table-filter relative d-inline-block">>><t><"row align-items-center"<"col-sm-6 text-left"p><"col-sm-6 text-sm-right"i>>',
           "pageLength": 6, 
           "bPaginate" : $('.data-table tbody tr').length>6,
           "iDisplayLength": 6,
           "language": {
                "search": "",
                "searchPlaceholder": "Type in to Search",
                "info": "_START_ -_END_ of _TOTAL_",
                "infoEmpty": "",
                "infoFiltered": "( Total _MAX_  )",
                "paginate": {
                    "first":      "First",
                    "last":       "Last",
                    "next":       "Next",
                    "previous":   "Prev"
                },
            }
        });
        
        $(".data-table-filter").append('<a href="#" class="btn btn-light-alt btn-xs btn-icon toggle-tigger"> <em class="ti ti-settings"></em> </a><div class="toggle-class toggle-datatable-filter dropdown-content dropdown-content-top-left text-left"><ul class="pdt-1x pdb-1x"><li class="pd-1x pdl-2x pdr-2x"> <input class="data-filter input-checkbox input-checkbox-sm" type="radio" name="filter" id="all" checked value=""> <label for="all">All</label></li><li class="pd-1x pdl-2x pdr-2x"> <input class="data-filter input-checkbox input-checkbox-sm" type="radio" name="filter" id="approved" value="approved"> <label for="approved">Approved</label></li><li class="pd-1x pdl-2x pdr-2x"> <input class="data-filter input-checkbox input-checkbox-sm" type="radio" name="filter" value="pending" id="pending"> <label for="pending">Pending</label></li><li class="pd-1x pdl-2x pdr-2x"> <input class="data-filter input-checkbox input-checkbox-sm" type="radio" name="filter" value="progress" id="progress"> <label for="progress">Progress</label></li><li class="pd-1x pdl-2x pdr-2x"> <input class="data-filter input-checkbox input-checkbox-sm" type="radio" name="filter" value="cancled" id="cancled"> <label for="cancled">Cancled</label></li></ul></div>');
        
        var $data_filter = $('.data-filter');
        $data_filter.on('change', function(){
            var _thisval = $(this).val();
            $data_table_fltr.columns('.dt-tnxno').search( _thisval ? _thisval : '', true, false ).draw();
        });
    }
    
    // Line Chart
    var lineChart = 'tknSale';
    if ($('#'+lineChart).length > 0) {
        var lineCh = document.getElementById(lineChart).getContext("2d");
        
        var chart = new Chart(lineCh, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: ["01 Oct", "02 Oct", "03 Oct", "04 Oct", "05 Oct", "06 Oct", "07 Oct"],
                datasets: [{
                    label: "",
                    tension:0.4,
                    backgroundColor: 'transparent',
                    borderColor: '#2c80ff',
                    pointBorderColor: "#2c80ff",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 2,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "#2c80ff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 6,
                    pointHitRadius: 6,
                    data: [110, 80, 125, 55, 95, 75, 90],
                }]
            },

            // Configuration options go here
            options: {
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return 'Date : ' + data['labels'][tooltipItem[0]['index']];
                        },
                        label: function(tooltipItem, data) {
                            return data['datasets'][0]['data'][tooltipItem['index']] + ' Tokens' ;
                        }
                    },
                    backgroundColor: '#eff6ff',
                    titleFontSize: 13,
                    titleFontColor: '#6783b8',
                    titleMarginBottom:10,
                    bodyFontColor: '#9eaecf',
                    bodyFontSize: 14,
                    bodySpacing:4,
                    yPadding: 15,
                    xPadding: 15,
                    footerMarginTop: 5,
                    displayColors: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            fontSize:12,
                            fontColor:'#9eaecf',
                            
                        },
                        gridLines: { 
                            color: "#e5ecf8",
                            tickMarkLength:0,
                            zeroLineColor: '#e5ecf8'
                        },
                        
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize:12,
                            fontColor:'#9eaecf',
                            source: 'auto',
                        },
                        gridLines: {
                            color: "transparent",
                            tickMarkLength:20,
                            zeroLineColor: '#e5ecf8',
                        },
                    }]
                }
            }
        });
    }
    
    // Bootstrap Modal Fix
    var $modal = $('.modal');
    $modal.on('shown.bs.modal', function () {
        if(!$body.hasClass('modal-open'))  {
            $body.addClass('modal-open');
        }
    });

    $('#idProveedor').change(function () {
        if ($(this).val() != "" ) {
            $("#cif_c").val("");
            $("#name").val("");
            $("#billing_address_street").val("");
            $("#billing_address_city").val("");
            $("#billing_address_state").val("");
            $("#billing_address_postalcode").val("");
            $("#billing_address_country").val("");
            $("#iban").val("");
            $("#phone").val("");
            $("#contact").val("");

            $("#cif_c").removeAttr("readonly");
            $("#name").removeAttr("readonly");
            $("#billing_address_street").removeAttr("readonly");
            $("#billing_address_city").removeAttr("readonly");
            $("#billing_address_state").removeAttr("readonly");
            $("#billing_address_postalcode").removeAttr("readonly");
            $("#billing_address_country").removeAttr("readonly");    
            $("#iban").removeAttr("readonly");
            $("#phone").removeAttr("readonly");
            $("#contact").removeAttr("readonly");

            if($(this).val() != "addprovider"){
                $("#email").attr("readonly", "readonly");
                $("#phone").val("");
                $("#contact").val("");
                $("#phone").attr("readonly", "readonly");
                $("#contact").attr("readonly", "readonly");
                $("#phone").parent().hide();
                $("#contact").parent().hide();

                $.ajax({
                    type: 'GET',
                    url: BASE_URL + '/finance/provider/' + $(this).val(),
                    success: function(response){
                        console.log(response);

                        $("#cif_c").val(response.cif_c);
                        $("#name").val(response.name);
                        $("#email").val(response.email1);
                        $("#billing_address_street").val(response.billing_address_street);
                        $("#billing_address_city").val(response.billing_address_city);
                        $("#billing_address_state").val(response.billing_address_state);
                        $("#billing_address_postalcode").val(response.billing_address_postalcode);
                        $("#billing_address_country").val(response.billing_address_country);
                        $("#iban").val(response.iban_c);

                        $("#cif_c").parent().show();
                        $("#name").parent().show();
                        $("#email").parent().show();
                        $("#billing_address_street").parent().show();
                        $("#billing_address_city").parent().show();
                        $("#billing_address_state").parent().show();
                        $("#billing_address_postalcode").parent().show();
                        $("#billing_address_country").parent().show();
                        $("#iban").parent().show();
                    },
                    dataType: 'json'
                });
            }else{
                $("#email").val("");
                $("#email").removeAttr("readonly");

                $("#cif_c").parent().show();
                $("#name").parent().show();
                $("#email").parent().show();
                $("#billing_address_street").parent().show();
                $("#billing_address_city").parent().show();
                $("#billing_address_state").parent().show();
                $("#billing_address_postalcode").parent().show();
                $("#billing_address_country").parent().show();
                $("#iban").parent().show();
                $("#phone").parent().show();
                $("#contact").parent().show();
            }

        } else {
            $("#cif_c").val("");
            $("#name").val("");
            $("#email").val("");
            $("#billing_address_street").val("");
            $("#billing_address_city").val("");
            $("#billing_address_state").val("");
            $("#billing_address_postalcode").val("");
            $("#billing_address_country").val("");
            $("#iban").val("");
            $("#phone").val("");
            $("#contact").val("");

            $("#cif_c").attr("readonly", "readonly");
            $("#name").attr("readonly", "readonly");
            $("#billing_address_street").attr("readonly", "readonly");
            $("#billing_address_city").attr("readonly", "readonly");
            $("#billing_address_state").attr("readonly", "readonly");
            $("#billing_address_postalcode").attr("readonly", "readonly");
            $("#billing_address_country").attr("readonly", "readonly"); 
            $("#iban").attr("readonly", "readonly");
            $("#phone").attr("readonly", "readonly");
            $("#contact").attr("readonly", "readonly");

            $("#cif_c").parent().hide();
            $("#name").parent().hide();
            $("#email").parent().hide();
            $("#billing_address_street").parent().hide();
            $("#billing_address_city").parent().hide();
            $("#billing_address_state").parent().hide();
            $("#billing_address_postalcode").parent().hide();
            $("#billing_address_country").parent().hide(); 
            $("#iban").parent().hide();
            $("#phone").parent().hide();
            $("#contact").parent().hide();
        }
    });
    $('#idProveedor').change();

    $("#calculate").on("click", function(){
        if ($("#new_fecha_vencimiento_c").val() != "" ) {
            var to = fecha_vencimiento_c.split("-");
            var hoy = new Date(to[2],to[1],to[0]);
            var from = $("#new_fecha_vencimiento_c").val().split("-");
            var fechaFormulario = new Date(from[2],from[1],from[0]);

            if (hoy < fechaFormulario) {
                console.log($("#idInvoice").val())
                var data = {
                    new_fecha_vencimiento_c:$("#new_fecha_vencimiento_c").val(),
                    fecha_vencimiento_real_c:fecha_vencimiento_c,
                    idInvoice:$("#idInvoice").val(),
                    importe_c: importe_c,
                };
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/finance/calculate/',
                    async: true,
                    data: data,
                    dataType: 'json',
                    success: function(response){
                        console.log(response)
                        var formatimpTIN = response.impTIN.toString();
                        var formatcomAbertura = response.comAbertura.toString();
                        var formatretencion = response.retencion.toString();
                        var formatimpTotalLiquidar = response.impTotalLiquidar.toString();
                        $("#condiciones").show();
                        $("#empresa").show();
                        $("#solicitar_liquidacion").show(); 
                        $('#condiciones').prev().show();
                        $('#empresa').prev().show();
                        $("#date_entered").removeClass('show');
                        $("#date_entered").addClass('hide');
                        $("#date_entered").hide();

                        $("span.impTIN").html(formatimpTIN);
                        $("input.impTIN").val(response.impTIN);
                        $("span.comAbertura").html(formatcomAbertura);
                        $("input.comAbertura").val(response.comAbertura);
                        $("span.retencion").html(formatretencion);
                        $("input.retencion").val(response.retencion);
                        $("span.impTotalLiquidar").html(formatimpTotalLiquidar);
                        $("input.impTotalLiquidar").val(response.impTotalLiquidar);
                        $("input.totalComision").val(response.totalComision);
                        $("input.porComision").val(response.porComision);
                        $("span.fecha_nuevo_vencimiento").html($("#new_fecha_vencimiento_c").val());
                    },
                    error: function(){
                        $("#condiciones").hide();
                        $("#empresa").hide();
                        $("#solicitar_liquidacion").hide(); 
                        $('#condiciones').prev().hide();
                        $('#empresa').prev().hide();
                    }
                });
            }else{
                //alert('Advertencia!!. La fecha introducida no puede ser inferior a la Fecha Vencimiento actual');
                $("#new_fecha_vencimiento_c").val('');
                $("#date_entered").removeClass('hide');
                $("#date_entered").addClass('show');
                $("#date_entered").show();
            }           
        }else{
            $("#condiciones").hide();
            $("#empresa").hide();
            $('#condiciones').prev().hide();
            $('#empresa').prev().hide();
            $("#solicitar_liquidacion").hide(); 
            $("#date_entered").removeClass('show');
            $("#date_entered").addClass('hide');
            $("#date_entered").hide();
        }
    });
    $('#calculate').click();
})(jQuery);